DROP DATABASE IF EXISTS isuumo;
CREATE DATABASE isuumo;

DROP TABLE IF EXISTS isuumo.estate;
DROP TABLE IF EXISTS isuumo.chair;

CREATE TABLE isuumo.estate
(
    id           INTEGER        UNSIGNED     NOT NULL PRIMARY KEY,
    name         VARCHAR(64)                NOT NULL,
    description  VARCHAR(4096)              NOT NULL,
    thumbnail    VARCHAR(128)               NOT NULL,
    address      VARCHAR(128)               NOT NULL,
    latitude     DOUBLE         PRECISION   NOT NULL,
    longitude    DOUBLE         PRECISION   NOT NULL,
    rent         MEDIUMINT      UNSIGNED    NOT NULL,
    door_height  SMALLINT       UNSIGNED    NOT NULL,
    door_width   SMALLINT       UNSIGNED    NOT NULL,
    features     VARCHAR(64)                NOT NULL,
    popularity   INTEGER UNSIGNED           NOT NULL,
    estate_point POINT AS (POINT(latitude, longitude)) STORED NOT NULL,
    popularity_desc INTEGER AS (-popularity) NOT NULL
);

CREATE INDEX estate_rent_id ON isuumo.estate (rent, id);
CREATE INDEX estate_door_width_rent on isuumo.estate (door_width, rent);
CREATE INDEX estate_door_height_rent on isuumo.estate (door_height, rent);
CREATE INDEX estate_door_height_width_rent on isuumo.estate (door_height, door_width, rent);
CREATE INDEX estate_popularity_desc_id on isuumo.estate (popularity_desc, id);
CREATE SPATIAL INDEX estate_point_idx on isuumo.estate (estate_point);

CREATE TABLE isuumo.chair
(
    id          INTEGER         UNSIGNED         NOT NULL PRIMARY KEY,
    name        VARCHAR(64)                      NOT NULL,
    description VARCHAR(4096)                    NOT NULL,
    thumbnail   VARCHAR(128)                     NOT NULL,
    price       MEDIUMINT       UNSIGNED         NOT NULL,
    height      SMALLINT        UNSIGNED         NOT NULL,
    width       SMALLINT        UNSIGNED         NOT NULL,
    depth       SMALLINT        UNSIGNED         NOT NULL,
    color       VARCHAR(64)                      NOT NULL,
    features    VARCHAR(64)                      NOT NULL,
    kind        VARCHAR(64)                      NOT NULL,
    popularity  INTEGER         UNSIGNED         NOT NULL,
    stock       SMALLINT        UNSIGNED         NOT NULL,
    popularity_desc INTEGER AS (-popularity)     NOT NULL
);

CREATE INDEX chair_price_id ON isuumo.chair (price, id);
CREATE INDEX chair_stock_price_id ON isuumo.chair (stock, price, id);
CREATE INDEX chair_price_stock ON isuumo.chair (price, stock);
CREATE INDEX chair_popularity_id_price ON isuumo.chair (popularity, id, price);
CREATE INDEX chair_kind_stock ON isuumo.chair (kind, stock);
CREATE INDEX chair_color_stock ON isuumo.chair (color, stock);
CREATE INDEX chair_height_stock ON isuumo.chair (height, stock);
CREATE INDEX chair_width_stock ON isuumo.chair (width, stock);
CREATE INDEX chair_popularity_desc_id ON isuumo.chair (popularity_desc, id);
CREATE INDEX chair_depth_idx ON isuumo.chair (depth);
CREATE INDEX chair_depth_stock_idx ON isuumo.chair (depth, stock);
CREATE INDEX chair_price_height_stock ON isuumo.chair (price, height, stock);
CREATE INDEX chair_price_width_stock ON isuumo.chair (price, width, stock);
CREATE INDEX chair_price_kind_stock ON isuumo.chair (price, kind, stock);
